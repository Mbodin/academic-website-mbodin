page
en
Home page
eo
Ĉefpaĝo
fr
Page personnelle
end

menu
top.tree
end

# updating

description
en
	I am <span class = "name">Martin Bodin</span><sup><a href = "#familyName_en">1</a></sup>, member of the <a href = "https://team.inria.fr/spades/"><span class = "name">Spades</span></a> team, in <span class = "town">Grenoble</span>, at <a href = "https://www.inria.fr/en"><span class = "name">Inria</span></a>.
eo
	Mi estas <span class = "name">Martin Bodin</span><sup><a href = "#familyName_eo">1</a></sup>, skipano de <a href = "https://team.inria.fr/spades/"><span class = "name">Spades</span></a>, en <span class = "town">Grenoblo</span>, ĉe <a href = "https://www.inria.fr/"><span class = "name">Inria</span></a>.
fr
	Je suis <span class = "name">Martin Bodin</span><sup><a href = "#familyName_fr">1</a></sup>, membre de l’équipe <a href = "https://team.inria.fr/spades/"><span class = "name">Spades</span></a>, à <span class = "town">Grenoble</span>, à l’<a href = "https://www.inria.fr/"><span class = "name">Inria</span></a>.
end

description
en
	My research interests are mainly around programming languages design, formalisations, and analyses.
	I work with the <a href = "http://coq.inria.fr"><span class = "name">Coq</span> proof assistant</a>, which provides strong guarantees to the softwares I build.
	I am especially interested in applying formal methods to real-world programming languages.
	Such languages usually come with many special (possibly unexpected) behaviours, which can lead to serious programming mistakes.
	I believe that formal methods can help programmers detect and avoid these mistakes.
eo
	Mia esploradinteresoj ĉefe fokusiĝas pri programlingvaĵaj elkreoj, formalizigoj, kaj analizoj.
	Mi laboras kun la <a href = "http://coq.inria.fr"><span class = "name">Coq</span> pruvilo</a>, kiu alte fidigas la programaĵojn kiujn mi kreas.
	Mi specife interesiĝas pri apliki formalajn metodojn al industrie uzitajn programlingvaĵojn.
	Tiaj lingvaĵojn elmontremas specialajn (kaj kelkfoje neatenditajn) komportamentojn, kiuj povas konduki al gravaj programeraroj.
	Mi pensas ke formalaj metodoj povas helpi programistojn ekscii kaj eviti tiajn erarojn.
fr
	Mes thèmes de recherches se concentrent majoritairement autour de la conception, la formalisation, et les analyses de langages de programmation.
	Je travaille avec l’<a href = "http://coq.inria.fr">assistant de preuve <span class = "name">Coq</span></a>, qui garantit un haut niveau de confiance aux les programmes que je conçois.
	Je suis particulièrement intéressé par l’application des méthodes formelles aux langages de programmation fréquemment utilisés en industrie.
	Ces langages ont souvent des comportements spéciaux parfois inattendus, qui peuvent mener à des erreurs de programmation importantes.
	Je pense que les méthodes formelles peuvent aider les programmeurs à détecter et éviter ces erreurs.
end

description
en
	In my research, I thus formalised widely used programming languages (<a href = "https://github.com/jscert/jscert">JavaScript</a>, <a href = "https://github.com/Mbodin/coqR">R</a>, and <a href = "https://github.com/Imperial-Wasm/wasm-coq-public">WebAssembly</a>).
	These formalisations (with the exception of WebAssembly) are notably large compared to other formalisations from the field.
	I thus helped design <a href = "http://skeletons.inria.fr/">skeletons</a>, a formalism to express and derive formally-proven program analyses from such large formalisations.
	I hope that these projects will help designing practical tools for code-quality control in industry.
eo
	En mia esplorado, mi formaligis multuzitajn programlingvaĵojn (<a href = "https://github.com/jscert/jscert">JavaScript</a>, <a href = "https://github.com/Mbodin/coqR">R</a>, kaj <a href = "https://github.com/Imperial-Wasm/wasm-coq-public">WebAssembly</a>).
	Tiuj formaligadoj (krom WebAssembly) rimarkiĝas per siaj grandecoj kompare al aliaj formaligoj el la fako.
	Mi do helpis elkrei la <a href = "http://skeletons.inria.fr/">ostarojn</a>, iu formalaĵo por defini kaj elkrei formalpruvitajn programanalizojn el tiaj grandaj formaligoj.
	Mi esperas ke tiaj projektoj helpos krei praktikajn ilojn por industrie kontroli kodkvaliton.
fr
	Parmi mes travaux, j’ai ainsi formalisé des langages de programmation très utilisés (<a href = "https://github.com/jscert/jscert">JavaScript</a>, <a href = "https://github.com/Mbodin/coqR">R</a>, et <a href = "https://github.com/Imperial-Wasm/wasm-coq-public">WebAssembly</a>).
	Ces formalisations (à l’exception de WebAssembly) se distinguent des autres formalisations du domaine par leur taille importante.
	J’ai donc participé à la conception des <a href = "http://skeletons.inria.fr/">squelettes</a>, un formalisme pour exprimer et dériver des analyses de programmes certifiées à partir de telles formalisations.
	J’ai bon espoir que ces projets permettrons de concevoir des outils de controle de qualité de code pour l’industrie.
end

description
en
	I am currently working on a formalism called <a href = "http://skeletons.inria.fr/">skeletons</a> to represent the semantics of real-world programming languages (JavaScript, R, etc.).
	This formalism enables to derive with minimal effort a wide range of program analyses certified in the <a href = "http://coq.inria.fr"><span class = "name">Coq</span> proof assistant</a>.
#
	I am also working on a <a href = "https://github.com/Imperial-Wasm/wasm-coq-public">formalisation of <span class = "name">WebAssembly</span>/<span class = "name">Wasm</span></a> in <span class = "name">Coq</span>.
eo
	Mi nun laboras pri formalismo (kiu nomiĝas <a href = "http://skeletons.inria.fr/">ostaron</a>) por reprezenti la semantikon de vervivaj programlingvaĵoj (Javaskripto, R, ktp.).
	Ĉi tiu formalismo permesigas dedukti granddiversajn programanalizojn pruvitajn en la <a href = "http://coq.inria.fr"><span class = "name">Coq</span> pruvilo</a>.
#
	Mi ankaŭ laboras pri <a href = "https://github.com/Imperial-Wasm/wasm-coq-public">formaligado de <span class = "name">WebAssembly</span>/<span class = "name">Wasm</span></a> en <span class = "name">Coq</span>.
fr
	Je travaille actuellement sur un formalisme, <a href = "http://skeletons.inria.fr/">les squelettes</a>, représentant la sémantique de langages de programmation complexes (JavaScript, R, etc.).
	Ce formalisme permet de déduire avec un effort minimal toute une gamme d’analyses de programmes certifiées dans l’<a href = "http://coq.inria.fr">assistant de preuve <span class = "name">Coq</span></a>.
#
	Je travaille aussi sur une <a href = "https://github.com/Imperial-Wasm/wasm-coq-public">formalisation de <span class = "name">WebAssembly</span>/<span class = "name">Wasm</span></a> en <span class = "name">Coq</span>.
end

description
en
	I did my PhD at <a href = "https://www.inria.fr/en"><span class = "name">Inria</span> <span class = "town">Rennes</span></a> under the supervision of <a href = "http://people.rennes.inria.fr/Alan.Schmitt/"><span class = "name">Alan Schmitt</span></a> and <a href = "http://www.irisa.fr/celtique/jensen/"><span class = "name">Thomas Jensen</span></a>.
	My PhD was about formal analyses of the <span class = "name">JavaScript</span> programming language.
	More information can be found <a class = "switchLanguage_en internalLink" href = "doktorigxo/companion.html?lang=en#en">in my thesis companion</a>.
eo
	Mi doktoriĝis ĉe <a href = "https://www.inria.fr/"><span class = "name">Inria</span></a> <span class = "town">Rennes</span> superrigardite de <a href = "http://people.rennes.inria.fr/Alan.Schmitt/"><span class = "name">Alan Schmitt</span></a> kaj <a href = "http://www.irisa.fr/celtique/jensen/"><span class = "name">Thomas Jensen</span></a>.
	Mi tiam laboris pri formala analizo de la <span class = "name">Javaskripta</span> programlingvaĵo.
	Pli da informoj troviĝas <a class = "switchLanguage_eo internalLink" href = "doktorigxo/companion.html?lang=eo#eo">en mia kundoktoriĝdokumento</a>.
fr
	J’ai fait mon doctorat à <a href = "https://www.inria.fr/fr"><span class = "name">Inria</span></a>, à <span class = "town">Rennes</span>, sous la supervision d’<a href = "http://people.rennes.inria.fr/Alan.Schmitt/"><span class = "name">Alan Schmitt</span></a> et de <a href = "http://www.irisa.fr/celtique/jensen/"><span class = "name">Thomas Jensen</span></a>.
	Je travaillais alors sur l’analyse formelle du langage de programmation <span class = "name">JavaScript</span>.
	Plus d’informations peuvent se trouver <a class = "switchLanguage_fr internalLink" href = "doktorigxo/companion.html?lang=fr#fr">sur mon guide de thèse</a>.
end

description
en
	I did a postdoc in the <a href = "http://www.cmm.uchile.cl/"><span class = "name">CMM</span></a>, in <span class = "name">Santiago de Chile</span>, where I formalised the <a href = "https://www.r-project.org/"><span class = "name">R</span></a> programming language in <span class = "name">Coq</span>.
	The formalisation is available <a href = "https://github.com/Mbodin/CoqR">online</a>.
eo
	Mi postdoktoriĝis ĉe la <a href = "http://www.cmm.uchile.cl/"><span class = "name">CMM</span></a>, en <span class = "name">Santiago</span>, en <span class = "name">Ĉilio</span>.
	Mi tiam formaligis la programlingvaĵon <a href = "https://www.r-project.org/"><span class = "name">R</span></a> en <span class = "name">Coq</span>.
	La formaligado disponeblas <a href = "https://github.com/Mbodin/CoqR">rete</a>.
fr
	J’ai fait un post-doc au <a href = "http://www.cmm.uchile.cl/"><span class = "name">CMM</span></a>, à <span class = "name">Santiago</span>, au <span class = "name">Chili</span>.
	J’y ai formalisé le langage de programmation <a href = "https://www.r-project.org/"><span class = "name">R</span></a> en <span class = "name">Coq</span>.
	La formalisation est disponible <a href = "https://github.com/Mbodin/CoqR">en ligne</a>.
end

description
en
	I then did a postdoc at the <a href = "https://www.imperial.ac.uk/"><span class = "name">Imperial College</span></a>, in <span class = "town">London</span>, in the <a href = "https://vtss.doc.ic.ac.uk/">Verified Trustworthy Software Specification</a> research group.
	It led to two <span class = "name">Coq</span> formalisations: <a href = "https://gitlab.inria.fr/skeletons/Coq/tree/POPL2019">skeletal semantics</a>, and <a href = "https://github.com/WasmCert/WasmCert-Coq">WebAssembly</a>.
en
	Mi poste postdoktoriĝis ĉe <a href = "https://www.imperial.ac.uk/"><span class = "name">Imperial College</span></a>, en <span class = "town">Londono</span>, je la esplorada grupo <a href = "https://vtss.doc.ic.ac.uk/">Verified Trustworthy Software Specification</a>.
	Ĝi alkondukis al du <span class = "name">Coq</span>aj formaligaĵoj: pri <a href = "https://gitlab.inria.fr/skeletons/Coq/tree/POPL2019">ostaraj semantikoj</a>, kaj <a href = "https://github.com/WasmCert/WasmCert-Coq">WebAssembly</a>.
fr
	J’ai ensuite été post-doctorant à l’<a href = "https://www.imperial.ac.uk/"><span class = "name">Imperial College</span></a>, à <span class = "town">Londres</span>, dans le groupe de recherche <a href = "https://vtss.doc.ic.ac.uk/">Verified Trustworthy Software Specification</a>.
	Ce post-doc a conduit à deux formalisations <span class = "name">Coq</span>: une sur les <a href = "https://gitlab.inria.fr/skeletons/Coq/tree/POPL2019">sémantiques squelettiques</a>, et une sur <a href = "https://github.com/WasmCert/WasmCert-Coq">WebAssembly</a>.
end

description
en
    <small><span id = "familyName_en">¹</span> Pronounced <span class = "phonetics">/maʁtɛ̃ bodɛ̃/</span>.  I am also known as <span class = "name">Martin Constantino–Bodin</span> <span class = "phonetics">/maʁtɛ̃ kɔnstɐ̃ntino bodɛ̃/</span>.</small>
eo
    <small><span id = "familyName_eo">¹</span> Prononcita <span class = "phonetics">/maʁtɛ̃ bodɛ̃/</span>.  Mi ankaŭ estas konata kiel <span class = "name">Martin Constantino–Bodin</span> <span class = "phonetics">/maʁtɛ̃ kɔnstɐ̃ntino bodɛ̃/</span>.</small>
fr
    <small><span id = "familyName_fr">¹</span> Prononcé <span class = "phonetics">/maʁtɛ̃ bodɛ̃/</span>.  On me connait aussi sous le nom de <span class = "name">Martin Constantino–Bodin</span> <span class = "phonetics">/maʁtɛ̃ kɔnstɐ̃ntino bodɛ̃/</span>.</small>
end

specdescription
en
	<dl class = "tableDescription">
		<dt>Current position</dt>
		<dd>Researcher at <a href = "https://www.inria.fr/en"><span class = "name">Inria</span></a>, in <span class = "town">Grenoble</span>.</dd>
		<dt>Team</dt>
		<dd><a href = "https://team.inria.fr/spades/">Spades</a></dd>
		<dt>CV</dt>
		<dd><a href = "rimedoj/cv_en.pdf" rel = "nofollow">Available there</a></dd>
		<dt>Public keys</dt>
		<dd>
			<ul class = "blocks">
				<li><a href = "rimedoj/id_rsa.pub" rel = "nofollow">SSH</a></li>
				<li><a href = "rimedoj/0x6CC4BC7F.asc" rel = "nofollow">OpenPGP</a></li>
			</ul>
    </dd>
		<dt>Internet presence</dt>
		<dd>
			<ul class = "blocks">
				<li><a href = "http://orcid.org/0000-0003-3588-3782"><span class = "name">ORCID</span></a></li>
				<li><a href = "http://dblp.uni-trier.de/pers/hd/b/Bodin:Martin.html"><span class = "name">DBLP</span></a></li>
				<li><a href = "http://scholar.google.fr/citations?user=OoWGEVEAAAAJ&amp;hl=en"><span class = "name">Google Scholar</span></a></li>
				<li><a href = "https://github.com/Mbodin"><span class = "name">GitHub</span></a></li>
				<li><a href = "http://en.wikipedia.org/wiki/User:Greatfermat"><span class = "name">Wikipedia</span></a></li>
				<li><a href = "http://en.wiktionary.org/wiki/User:Greatfermat"><span class = "name">Wiktionary</span></a></li>
				<li><a href = "https://www.openstreetmap.org/user/Martin%20Constantino%E2%80%93Bodin"><span class = "name">OpenStreetMap</span></a></li>
			</ul>
		</dd>
	</dl>
eo
	<dl class = "tableDescription">
		<dt>Nuna pozicio</dt>
		<dd>Esploradisto ĉe <a href = "https://inria.fr"><span class = "name">Inria</span></a>, en <span class = "town">Grenoble</span>.</dd>
		<dt>Skipo</dt>
		<dd><a href = "https://team.inria.fr/spades/">Spades</a></dd>
		<dt>Vivresumo</dt>
		<dd><a href = "rimedoj/cv_eo.pdf" rel = "nofollow">Havebla tie ĉi</a></dd>
		<dt>Publikaj ŝlosiloj</dt>
		<dd>
			<ul class = "blocks">
				<li><a href = "rimedoj/id_rsa.pub" rel = "nofollow">SSH</a></li>
				<li><a href = "rimedoj/0x6CC4BC7F.asc" rel = "nofollow">OpenPGP</a></li>
			</ul>
		</dd>
		<dt>Interreta ĉeesto</dt>
		<dd>
			<ul class = "blocks">
				<li><a href = "http://orcid.org/0000-0003-3588-3782"><span class = "name">ORCID</span></a></li>
				<li><a href = "http://dblp.uni-trier.de/pers/hd/b/Bodin:Martin.html"><span class = "name">DBLP</span></a></li>
				<li><a href = "http://scholar.google.fr/citations?user=OoWGEVEAAAAJ&amp;hl=eo"><span class = "name">Google Scholar</span></a></li>
				<li><a href = "https://github.com/Mbodin"><span class = "name">GitHub</span></a></li>
				<li><a href = "http://eo.wikipedia.org/wiki/Uzanto:Greatfermat"><span class = "name">Vikipedio</span></a></li>
				<li><a href = "http://eo.wiktionary.org/wiki/Uzanto:Greatfermat"><span class = "name">Vikivortaro</span></a></li>
				<li><a href = "https://www.openstreetmap.org/user/Martin%20Constantino%E2%80%93Bodin"><span class = "name">OpenStreetMap</span></a></li>
			</ul>
		</dd>
	</dl>
fr
	<dl class = "tableDescription">
		<dt>Position actuelle</dt>
		<dd>Chargé de recherche à l’<a href = "https://www.inria.fr/fr"><span class = "name">Inria</span></a>, à <span class = "town">Grenoble</span>.</dd>
		<dt>Équipe</dt>
		<dd><a href = "https://team.inria.fr/spades/">Spades</a></dd>
		<dt>CV</dt>
		<dd><a href = "rimedoj/cv_fr.pdf" rel = "nofollow">Disponible ici</a></dd>
		<dt>Clefs publiques</dt>
		<dd>
			<ul class = "blocks">
				<li><a href = "rimedoj/id_rsa.pub" rel = "nofollow">SSH</a></li>
				<li><a href = "rimedoj/0x6CC4BC7F.asc" rel = "nofollow">OpenPGP</a></li>
			</ul>
		</dd>
		<dt>Présence sur Internet</dt>
		<dd>
			<ul class = "blocks">
				<li><a href = "http://orcid.org/0000-0003-3588-3782"><span class = "name">ORCID</span></a></li>
				<li><a href = "http://dblp.uni-trier.de/pers/hd/b/Bodin:Martin.html"><span class = "name">DBLP</span></a></li>
				<li><a href = "http://scholar.google.fr/citations?user=OoWGEVEAAAAJ&amp;hl=fr"><span class = "name">Google Scholar</span></a></li>
				<li><a href = "https://github.com/Mbodin"><span class = "name">GitHub</span></a></li>
				<li><a href = "http://fr.wikipedia.org/wiki/Utilisateur:Greatfermat"><span class = "name">Wikipédia</span></a></li>
				<li><a href = "http://fr.wiktionary.org/wiki/Utilisateur:Greatfermat"><span class = "name">Wiktionnaire</span></a></li>
				<li><a href = "https://www.openstreetmap.org/user/Martin%20Constantino%E2%80%93Bodin"><span class = "name">OpenStreetMap</span></a></li>
			</ul>
		</dd>
	</dl>
end

rightImage
	<a href = "javascript:void 42"><img id = "imageOfMe" src = "rimedoj/moi.png" alt = "Me — Moi — Mi" /></a> <!-- TODO:  Change this “alt” property according to the language. -->
	<script type = "text/javascript" src = "skriptoj/imageOfMe.js" async = "async"></script>
end

